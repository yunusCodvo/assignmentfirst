import React from 'react';
import ExpenseItem from './ExpenseItem';
import './Expense.css';
const Expense = (props) => {
  return (
    <div className="expenses">
      {props?.expense.length != 0 ? (
        props?.expense?.map((expense) => (
          <ExpenseItem
            key={expense.id}
            date={expense.date}
            title={expense.title}
            amount={expense.amount}
          />
        ))
      ) : (
        <p>No Expense Found</p>
      )}
    </div>
  );
};

export default Expense;
