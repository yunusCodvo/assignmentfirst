import React from 'react';
import Expense from './components/Expense';

const expenses = [
  {
    id: 'a1',
    title: 'Laptop',
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  { id: 'b2', title: 'New TV', amount: 799.49, date: new Date(2021, 2, 12) },
  {
    id: 'c3',
    title: 'Mobile',
    amount: 294.67,
    date: new Date(2019, 2, 28),
  },
  {
    id: 'd4',
    title: 'New Desk (Wooden)',
    amount: 450,
    date: new Date(2022, 5, 12),
  },
];
function App() {
  return (
    <div>
      <h2 style={{ textAlign: 'center', color: '#fff' }}>Expense List</h2>
      <Expense expense={expenses} />
    </div>
  );
}

export default App;
